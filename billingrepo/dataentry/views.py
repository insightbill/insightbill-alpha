# -*- coding: utf-8 -*-
from __future__ import absolute_import, unicode_literals

from django.core.urlresolvers import reverse
from django.db.models import Q
from django.shortcuts import render
from django.views.generic import DetailView, ListView, RedirectView, UpdateView

from braces.views import LoginRequiredMixin

from .models import Patient, Site
from .forms import PatientForm, SiteForm


nav = "dataentry"
sidebar_items = [
        {"name": "patients",
         "url": "dataentry:patient_list"},
        {"name": "sites",
         "url": "dataentry:site_list"},
]


# Create your views here.
class SiteDetailView(DetailView):
    model = Site
    slug_field = "id"
    slug_url_kwarg = "id"

    def get_context_data(self, **kwargs):
        context = super(SiteDetailView, self).get_context_data(**kwargs)
        context['nav'] = nav
        context['side'] = "sites"
        context['sidebar_items'] = sidebar_items
        return context


class SiteRedirectView(LoginRequiredMixin, RedirectView):
    permanent = False

    def get_redirect_url(self):
        return reverse("dataentry:site_detail",
                       kwargs={"id": self.request.site.id})


class SiteUpdateView(LoginRequiredMixin, UpdateView):
    model = Site
    fields = ['name', 'address1', 'address2', 'city', 'state', 'zip_code',
              'phone_no', 'fax_no', 'status', ]
    slug_field = "id"
    slug_url_kwarg = "id"

    def get_context_data(self, **kwargs):
        context = super(SiteUpdateView, self).get_context_data(**kwargs)
        context['nav'] = nav
        context['side'] = "sites"
        context['sidebar_items'] = sidebar_items
        return context


class SiteListView(LoginRequiredMixin, ListView):
    model = Site
    list_display = ('last_name', 'first_name', 'birth_date', 'age', 'status', )
    search_fields = ('last_name', 'first_name', 'birth_date', )

    def get_object(self, queryset=None):
        obj = self.objects.get(id=self.kwargs['id'])
        return obj

    def get_context_data(self, **kwargs):
        context = super(SiteListView, self).get_context_data(**kwargs)
        context['nav'] = nav
        context['side'] = "sites"
        context['sidebar_items'] = sidebar_items
        return context


def site_entry(request):
    title = "Site Data Entry"
    if request.user.is_authenticated():
        usrnam = request.user
    else:
        usrnam = "N/A"
    form = SiteForm(request.POST or None)

    context = {
        "title": title,
        "msg": "Logged in as: %s" % usrnam,
        "nav": nav,
        "side": "sites",
        "sidebar_items": sidebar_items,
        "form": form,
    }

    if form.is_valid():
        instance = form.save(commit=False)
        instance.save()
        context = {
            "msg": "Saved",
        }
    return render(request, 'forms.html', context)


class PatientDetailView(LoginRequiredMixin, DetailView):
    model = Patient
    slug_field = "pid"
    slug_url_kwarg = "pid"

    def get_context_data(self, **kwargs):
        context = super(PatientDetailView, self).get_context_data(**kwargs)
        context['nav'] = nav
        context['side'] = "patients"
        context['sidebar_items'] = sidebar_items
        return context


class PatientRedirectView(LoginRequiredMixin, RedirectView):
    permanent = False

    def get_redirect_url(self):
        return reverse("dataentry:detail",
                       kwargs={"pid": self.request.patient.pid})


class PatientUpdateView(LoginRequiredMixin, UpdateView):

    model = Patient
    slug_field = "pid"
    slug_url_kwarg = "pid"
    fields = ['external_pid', 'ssn', 'first_name', 'last_name', 'mi',
              'birth_date', 'sex', 'address1', 'address2', 'city', 'state',
              'zip_code', 'phone_no', 'status', ]

    def get_context_data(self, **kwargs):
        context = super(PatientUpdateView, self).get_context_data(**kwargs)
        context['nav'] = nav
        context['side'] = "patients"
        context['sidebar_items'] = sidebar_items
        return context


class PatientListView(LoginRequiredMixin, ListView):
    model = Patient
    is_paginated = True
    paginate_by = 20
    queryset = Patient.objects.all()

    def get_context_data(self, **kwargs):
        context = super(PatientListView, self).get_context_data(**kwargs)
        context['nav'] = nav
        context['side'] = "patients"
        context['sidebar_items'] = sidebar_items
        return context

    def get_queryset(self):
        qs = super(PatientListView, self).get_queryset()

        query = self.request.GET.get("q")
        if query:
            qs = self.model.objects.filter(
                Q(last_name__icontains=query) |
                Q(first_name__icontains=query)
            )
        return qs


def NewPatient(request):
    title = "Patient Data Entry"
    if request.user.is_authenticated():
        usrnam = request.user
    else:
        usrnam = "N/A"
    form = PatientForm(request.POST or None)

    context = {
        "title": title,
        "msg": "Logged in as: %s" % usrnam,
        "nav": nav,
        "side": 'patients',
        "sidebar_items": sidebar_items,
        "form": form,
    }

    if form.is_valid():
        instance = form.save(commit=False)
        instance.save()
        context = {
            "msg": "Saved",
        }
    return render(request, 'dataentry/forms.html', context)
