from datetime import date
import uuid

from django.db import models
from django.core.urlresolvers import reverse
from localflavor.us import models as usmodels

from billingrepo.setup.models import ICD, Payor


# Create your models here.
class Patient(models.Model):
    pid = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    external_pid = models.CharField(max_length=25, blank=True)
# should try to store encrypted SSN
    ssn = usmodels.USSocialSecurityNumberField(blank=True)
    first_name = models.CharField(max_length=100)
    last_name = models.CharField(max_length=100)
    mi = models.CharField(max_length=4, blank=True)
    birth_date = models.DateField()
    sex = models.CharField(max_length=1, choices=(('M', 'Male'),
                                                  ('F', 'Female'),
                                                  ('O', 'Other')
                                                  ))
    address1 = models.CharField(max_length=100, blank=True)
    address2 = models.CharField(max_length=100, blank=True)
    city = models.CharField(max_length=100)
    state = usmodels.USStateField()
    zip_code = usmodels.USZipCodeField()
    phone_no = usmodels.PhoneNumberField(blank=True)
    status = models.CharField(max_length=20,
                              default='Draft', choices=(('Draft', 'Draft'),
                                                        ('Active', 'Active'),
                                                        ('Inactive', 'Inactive')
                                                        ))
    created = models.DateTimeField(auto_now_add=True, editable=False)
    updated = models.DateTimeField(auto_now=True, editable=False)

    def __str__(self):
        return str(self.pid) + ' - ' + ', '.join((self.last_name,
                                                  self.first_name))

    def age(self):
        """returns current age of patient"""
        today = date.today()
        return today.year - self.birth_date.year - ((today.month, today.day) < (self.birth_date.month, self.birth_date.day))

    def get_absolute_url(self):
        return reverse('demographics:patient_detail', kwargs={'pid': self.pid})


class PatientPayor(models.Model):
    """Relationship between patients and insurers"""
    patient = models.ForeignKey(Patient, on_delete=models.CASCADE)
    payor = models.ForeignKey(Payor, on_delete=models.CASCADE)
    policy_no = models.CharField(max_length=25, blank=True)
    group_no = models.CharField(max_length=25, blank=True)
    status = models.CharField(max_length=20,
                              default='Draft', choices=(('Draft', 'Draft'),
                                                        ('Active', 'Active'),
                                                        ('Inactive', 'Inactive')
                                                        ))
    start_date = models.DateField()
    end_date = models.DateField(blank=True)

    def is_expired(self):
        expired = False
        today = date.today()
        if self.end_date:
            expired = (self.end_date - today).days < 0
        return expired


class Site(models.Model):
    """Site of service"""
    name = models.CharField(max_length=255, unique=True)
    address1 = models.CharField(max_length=100, blank=True)
    address2 = models.CharField(max_length=100, blank=True)
    city = models.CharField(max_length=100)
    state = usmodels.USStateField()
    zip_code = usmodels.USZipCodeField()
    phone_no = usmodels.PhoneNumberField(blank=True)
    fax_no = usmodels.PhoneNumberField(blank=True)
    status = models.CharField(max_length=20,
                              default='Draft', choices=(('Draft', 'Draft'),
                                                        ('Active', 'Active'),
                                                        ('Inactive', 'Inactive')
                                                        ))

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse('demographics:site_detail', kwargs={'id': self.id})


class Order(models.Model):
    order_id = models.UUIDField(default=uuid.uuid4, editable=False)
    date_of_service = models.DateField()
    status = models.CharField(max_length=20, default='Draft',
                              choices=(('Draft', 'Draft'),
                                       ('Pending', 'Pending'),
                                       ('Complete', 'Complete'),
                                       ('Cancelled', 'Cancelled')
                                       ))
    complete_date = models.DateField(blank=True, null=True)		# assert
    site = models.ForeignKey(Site, on_delete=models.CASCADE)

    def __str__(self):
        return self.order_id


class OrderICD(models.Model):
    """Relationship between orders and ICD codes"""
    order = models.ForeignKey(Order, on_delete=models.CASCADE)
    icd = models.ManyToManyField(ICD)


class Contact(models.Model):
    """Doctors"""
    code = models.CharField(max_length=25, unique=True)
    first_name = models.CharField(max_length=100)
    last_name = models.CharField(max_length=100)
    mi = models.CharField(max_length=4, blank=True)
    status = models.CharField(max_length=20,
                              default='Draft', choices=(('Draft', 'Draft'),
                                                        ('Active', 'Active'),
                                                        ('Inactive', 'Inactive')
                                                        ))
    npi = models.CharField(max_length=10)
    address1 = models.CharField(max_length=100, blank=True)
    address2 = models.CharField(max_length=100, blank=True)
    city = models.CharField(max_length=100)
    state = usmodels.USStateField()
    zip_code = usmodels.USZipCodeField()
    phone_no = usmodels.PhoneNumberField(blank=True)
    fax_no = usmodels.PhoneNumberField(blank=True)

    def __str__(self):
        return "%s-%s" % (self.code, self.last_name)


class Client(models.Model):
    """Doctors' Offices"""
    code = models.CharField(max_length=25, unique=True)
    name = models.CharField(max_length=100)
    status = models.CharField(max_length=20,
                              default='Draft', choices=(('Draft', 'Draft'),
                                                        ('Active', 'Active'),
                                                        ('Inactive', 'Inactive')
                                                        ))
    npi = models.CharField(max_length=10)
    address1 = models.CharField(max_length=100, blank=True)
    address2 = models.CharField(max_length=100, blank=True)
    city = models.CharField(max_length=100)
    state = usmodels.USStateField()
    zip_code = usmodels.USZipCodeField()
    phone_no = usmodels.PhoneNumberField(blank=True)
    fax_no = usmodels.PhoneNumberField(blank=True)

    def __str__(self):
        return self.code


class ClientContact(models.Model):
    """Relationship between Doctors and Offices"""
    client = models.ForeignKey(Client, on_delete=models.CASCADE)
    contact = models.ManyToManyField(Contact)
    start_date = models.DateField(auto_now=True)
    end_date = models.DateField(blank=True)

    def is_expired(self):
        expired = False
        today = date.today()
        if self.end_date:
            expired = (self.end_date - today).days < 0
        return expired


class Claim(models.Model):
    """Claims made"""
    claim_id = models.UUIDField(default=uuid.uuid4, editable=False)
    order = models.ManyToManyField(Order)
    patient = models.ForeignKey(Patient, on_delete=models.CASCADE)
    contact = models.ForeignKey(Contact, on_delete=models.CASCADE)
    payor = models.ForeignKey(Payor, on_delete=models.CASCADE)
    status = models.CharField(max_length=20,
                              default='Draft', choices=(('Draft', 'Draft'),
                                                        ('Active', 'Active'),
                                                        ('Inactive', 'Inactive')
                                                        ))
    claim_date = models.DateField(blank=True, null=True)
    remittance_id = models.CharField(max_length=25, blank=True, null=True)

    def __str__(self):
        return self.claim_id
