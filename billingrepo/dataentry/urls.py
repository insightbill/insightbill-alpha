# -*- coding: utf-8 -*-
from __future__ import absolute_import, unicode_literals

from django.conf.urls import url
from django.views.generic import TemplateView

from . import views


urlpatterns = [
    url(r'^$', TemplateView.as_view(template_name='pages/dataentry_home.html'),
        name="home"),
    # URL pattern for the PatientListView
    url(
        regex=r'^patients/$',
        view=views.PatientListView.as_view(),
        name='patient_list'
    ),

    # URL pattern for a new patient
    url(
        regex=r'^patients/new/$',
        view=views.NewPatient,
        name='patient_new'
    ),

    # URL pattern for the PatientRedirectView
    url(
        regex=r'^patients/~redirect/$',
        view=views.PatientRedirectView.as_view(),
        name='patient_redirect'
    ),

    # URL pattern for the PatientDetailView
    url(
        regex=r'^patients/(?P<pid>[\w.@+-]+)/$',
        view=views.PatientDetailView.as_view(),
        name='patient_detail'
    ),

    # URL pattern for the PatientUpdateView
    url(
        regex=r'^patients/update/(?P<pid>[\w.@+-]+)/$',
        view=views.PatientUpdateView.as_view(),
        name="patient_update"),
    # sites
    url(regex=r'^sites/$',
        view=views.SiteListView.as_view(),
        name="site_list"),
    url(regex=r'^sites/new/$',
        view=views.site_entry,
        name="site_entry"),
    url(regex=r'^sites/~redirect/$',
        view=views.SiteRedirectView.as_view(),
        name="site_redirect"),
    url(regex=r'^sites/(?P<id>[\w.@+-]+)/$',
        view=views.SiteDetailView.as_view(),
        name="site_detail"),
    url(regex=r'^sites/update/(?P<id>[\w.@+-]+)$',
        view=views.SiteUpdateView.as_view(),
        name="site_update"),
]
