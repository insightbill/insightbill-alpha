from django import forms

from .models import Patient, Site, Order, Contact, Client, Claim, ClientContact


class PatientForm(forms.ModelForm):
    class Meta:
        model = Patient
        exclude = ['pid', ]


class SiteForm(forms.ModelForm):
    class Meta:
        model = Site
        exclude = ['id', ]


class OrderForm(forms.ModelForm):
    class Meta:
        model = Order
        exclude = ['order_id', ]


class ContactForm(forms.ModelForm):
    class Meta:
        model = Contact
        exclude = ['id', ]


class ClaimForm(forms.ModelForm):
    class Meta:
        model = Claim
        exclude = ['claim_id', ]


class ClientForm(forms.ModelForm):
    class Meta:
        model = Client
        exclude = ['id', ]


class ClientContactForm(forms.ModelForm):
    class Meta:
        model = ClientContact
        exclude = ['id', ]
