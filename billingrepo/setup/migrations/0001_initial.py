# -*- coding: utf-8 -*-
# Generated by Django 1.9.4 on 2016-03-11 05:41
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion
import localflavor.us.models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='CPT',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('code', models.CharField(max_length=25)),
                ('description', models.TextField(max_length=255)),
                ('version', models.CharField(max_length=10)),
                ('status', models.CharField(choices=[('Draft', 'Draft'), ('Active', 'Active'), ('Inactive', 'Inactive')], default='Draft', max_length=20)),
                ('start_date', models.DateField(auto_now=True)),
                ('end_date', models.DateField(blank=True)),
            ],
        ),
        migrations.CreateModel(
            name='ICD',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('code', models.CharField(max_length=25)),
                ('description', models.CharField(max_length=255)),
                ('version', models.CharField(max_length=10)),
                ('status', models.CharField(choices=[('Draft', 'Draft'), ('Active', 'Active'), ('Inactive', 'Inactive')], default='Draft', max_length=20)),
                ('start_date', models.DateField(auto_now=True)),
                ('end_date', models.DateField(blank=True)),
            ],
        ),
        migrations.CreateModel(
            name='Panel',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=255)),
                ('is_billable', models.BooleanField(default=True)),
                ('status', models.CharField(choices=[('Draft', 'Draft'), ('Active', 'Active'), ('Inactive', 'Inactive')], default='Draft', max_length=20)),
                ('start_date', models.DateField()),
                ('end_date', models.DateField(blank=True, null=True)),
            ],
        ),
        migrations.CreateModel(
            name='Payor',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=255, unique=True)),
                ('address1', models.CharField(blank=True, max_length=100)),
                ('address2', models.CharField(blank=True, max_length=100)),
                ('city', models.CharField(max_length=100)),
                ('state', localflavor.us.models.USStateField(choices=[('AL', 'Alabama'), ('AK', 'Alaska'), ('AS', 'American Samoa'), ('AZ', 'Arizona'), ('AR', 'Arkansas'), ('AA', 'Armed Forces Americas'), ('AE', 'Armed Forces Europe'), ('AP', 'Armed Forces Pacific'), ('CA', 'California'), ('CO', 'Colorado'), ('CT', 'Connecticut'), ('DE', 'Delaware'), ('DC', 'District of Columbia'), ('FL', 'Florida'), ('GA', 'Georgia'), ('GU', 'Guam'), ('HI', 'Hawaii'), ('ID', 'Idaho'), ('IL', 'Illinois'), ('IN', 'Indiana'), ('IA', 'Iowa'), ('KS', 'Kansas'), ('KY', 'Kentucky'), ('LA', 'Louisiana'), ('ME', 'Maine'), ('MD', 'Maryland'), ('MA', 'Massachusetts'), ('MI', 'Michigan'), ('MN', 'Minnesota'), ('MS', 'Mississippi'), ('MO', 'Missouri'), ('MT', 'Montana'), ('NE', 'Nebraska'), ('NV', 'Nevada'), ('NH', 'New Hampshire'), ('NJ', 'New Jersey'), ('NM', 'New Mexico'), ('NY', 'New York'), ('NC', 'North Carolina'), ('ND', 'North Dakota'), ('MP', 'Northern Mariana Islands'), ('OH', 'Ohio'), ('OK', 'Oklahoma'), ('OR', 'Oregon'), ('PA', 'Pennsylvania'), ('PR', 'Puerto Rico'), ('RI', 'Rhode Island'), ('SC', 'South Carolina'), ('SD', 'South Dakota'), ('TN', 'Tennessee'), ('TX', 'Texas'), ('UT', 'Utah'), ('VT', 'Vermont'), ('VI', 'Virgin Islands'), ('VA', 'Virginia'), ('WA', 'Washington'), ('WV', 'West Virginia'), ('WI', 'Wisconsin'), ('WY', 'Wyoming')], max_length=2)),
                ('zip_code', localflavor.us.models.USZipCodeField(max_length=10)),
                ('phone_no', localflavor.us.models.PhoneNumberField(blank=True, max_length=20)),
                ('fax_no', localflavor.us.models.PhoneNumberField(blank=True, max_length=20)),
                ('status', models.CharField(choices=[('Draft', 'Draft'), ('Active', 'Active'), ('Inactive', 'Inactive')], default='Draft', max_length=20)),
            ],
        ),
        migrations.CreateModel(
            name='PriceSheet',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('group_no', models.CharField(blank=True, max_length=25)),
                ('status', models.CharField(choices=[('Draft', 'Draft'), ('Active', 'Active'), ('Inactive', 'Inactive')], default='Draft', max_length=20)),
                ('start_date', models.DateField()),
                ('end_date', models.DateField(blank=True, null=True)),
                ('price', models.DecimalField(decimal_places=2, max_digits=7)),
                ('discount', models.DecimalField(decimal_places=2, default=0, max_digits=5)),
                ('cpt', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='setup.CPT')),
                ('panel', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='setup.Panel')),
                ('payor', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='setup.Payor')),
            ],
        ),
        migrations.CreateModel(
            name='Test',
            fields=[
                ('test_id', models.AutoField(primary_key=True, serialize=False)),
                ('test_name', models.CharField(max_length=50)),
                ('status', models.CharField(choices=[('Draft', 'Draft'), ('Active', 'Active'), ('Inactive', 'Inactive')], default='Draft', max_length=20)),
                ('start_date', models.DateField(auto_now=True)),
                ('end_date', models.DateField(blank=True, null=True)),
                ('cpt', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='setup.CPT')),
            ],
        ),
        migrations.AddField(
            model_name='pricesheet',
            name='test',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='setup.Test'),
        ),
        migrations.AddField(
            model_name='panel',
            name='test',
            field=models.ManyToManyField(to='setup.Test'),
        ),
    ]
