from datetime import date

from django.db import models
from localflavor.us import models as usmodels


class CPT(models.Model):
    """CPT Codes"""
    code = models.CharField(max_length=25)
    description = models.TextField(max_length=255)
    version = models.CharField(max_length=10)
    status = models.CharField(max_length=20,
                              default='Draft', choices=(('Draft', 'Draft'),
                                                        ('Active', 'Active'),
                                                        ('Inactive', 'Inactive')
                                                        ))
    start_date = models.DateField(auto_now=True)
    end_date = models.DateField(blank=True)

    def is_expired(self):
        expired = False
        today = date.today()
        if self.end_date:
            expired = (self.end_date - today).days < 0
        return expired

    def __str__(self):
        return self.code


class ICD(models.Model):
    """ICD Codes"""
    code = models.CharField(max_length=25)
    description = models.CharField(max_length=255)
    version = models.CharField(max_length=10)
    status = models.CharField(max_length=20,
                              default='Draft', choices=(('Draft', 'Draft'),
                                                        ('Active', 'Active'),
                                                        ('Inactive', 'Inactive')
                                                        ))
    start_date = models.DateField(auto_now=True)
    end_date = models.DateField(blank=True)

    def is_expired(self):
        expired = False
        today = date.today()
        if self.end_date:
            expired = (self.end_date - today).days < 0
        return expired

    def __str__(self):
        return self.code


class Test(models.Model):
    """Tests"""
    test_id = models.AutoField(primary_key=True)
    test_name = models.CharField(max_length=50)
    status = models.CharField(max_length=20,
                              default='Draft', choices=(('Draft', 'Draft'),
                                                        ('Active', 'Active'),
                                                        ('Inactive', 'Inactive')
                                                        ))
    start_date = models.DateField(auto_now=True)
    end_date = models.DateField(blank=True, null=True)
    cpt = models.ForeignKey(CPT, on_delete=models.CASCADE)

    def is_expired(self):
        expired = False
        today = date.today()
        if self.end_date:
            expired = (self.end_date - today).days < 0
        return expired

    def __str__(self):
        return "%s-%s" % (str(self.test_id), self.test_name)


class Panel(models.Model):
    """Panels"""
    name = models.CharField(max_length=255)
    test = models.ManyToManyField(Test)
    is_billable = models.BooleanField(default=True)
    status = models.CharField(max_length=20,
                              default='Draft', choices=(('Draft', 'Draft'),
                                                        ('Active', 'Active'),
                                                        ('Inactive', 'Inactive')
                                                        ))
    start_date = models.DateField()
    end_date = models.DateField(blank=True, null=True)

    def is_expired(self):
        expired = False
        today = date.today()
        if self.end_date:
            expired = (self.end_date - today).days < 0
        return expired

    def __str__(self):
        return self.name


class Payor(models.Model):
    """Insurers"""
    name = models.CharField(max_length=255, unique=True)
    address1 = models.CharField(max_length=100, blank=True)
    address2 = models.CharField(max_length=100, blank=True)
    city = models.CharField(max_length=100)
    state = usmodels.USStateField()
    zip_code = usmodels.USZipCodeField()
    phone_no = usmodels.PhoneNumberField(blank=True)
    fax_no = usmodels.PhoneNumberField(blank=True)
    status = models.CharField(max_length=20,
                              default='Draft', choices=(('Draft', 'Draft'),
                                                        ('Active', 'Active'),
                                                        ('Inactive', 'Inactive')
                                                        ))

    def __str__(self):
        return self.name


class PriceSheet(models.Model):
    """Prices vs  Tests"""
    payor = models.ForeignKey(Payor, on_delete=models.CASCADE)
    group_no = models.CharField(max_length=25, blank=True)
    status = models.CharField(max_length=20,
                              default='Draft', choices=(('Draft', 'Draft'),
                                                        ('Active', 'Active'),
                                                        ('Inactive', 'Inactive')
                                                        ))
    start_date = models.DateField()
    end_date = models.DateField(blank=True, null=True)
    cpt = models.ForeignKey(CPT, on_delete=models.CASCADE)
    panel = models.ForeignKey(Panel, on_delete=models.CASCADE)
    test = models.ForeignKey(Test, on_delete=models.CASCADE)
    price = models.DecimalField(max_digits=7, decimal_places=2)
    discount = models.DecimalField(default=0, max_digits=5, decimal_places=2)

    def is_expired(self):
        expired = False
        today = date.today()
        if self.end_date:
            expired = (self.end_date - today).days < 0
        return expired

    def __str__(self):
        return self.price_id
