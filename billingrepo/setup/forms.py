from django import forms

from .models import ICD, CPT, Test, Panel, Payor, PriceSheet


class ICDForm(forms.ModelForm):
    class Meta:
        model = ICD
        fields = ['code', 'description', 'version', 'status', 'end_date']


class CPTForm(forms.ModelForm):
    class Meta:
        model = CPT
        fields = ['code', 'description', 'version', 'status', 'end_date']


class TestForm(forms.ModelForm):
    class Meta:
        model = Test
        exclude = []


class PanelForm(forms.ModelForm):
    class Meta:
        model = Panel
        exclude = ['id', ]


class PayorForm(forms.ModelForm):
    class Meta:
        model = Panel
        exclude = ['id']


class PriceSheetForm(forms.ModelForm):
    class Meta:
        model = PriceSheet
        exclude = []


class UploadForm(forms.Form):
    type = forms.ChoiceField(required=True,
           choices=(('ICD', 'ICD'), ('Payors', 'Payors'), ('Tests', 'Tests'),
                    ('CPT', 'CPT'), ('Panels', 'Panels'), ))
    file = forms.FileField(required=True, label="Select File to upload")
