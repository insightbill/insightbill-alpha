import os
import shutil
from django.db import models
from csvImporter.model import CsvDbModel

from .models import ICD, CPT, Payor, Test

class IcdCsvImporter(CsvDbModel):
    def __init__(self):
        return

    code = models.CharField()
    description = models.CharField()
    version = models.CharField()
    status = models.CharField()
    start_date = models.DateField()
    end_date = models.DateField()


    class Meta:
        dbModel = ICD
        delimiter = ','


    def ImportFile(self, file_path):
        try:
            my_file = self.import_data(data=open(file_path))
            return True
        except:
            return False


class CPTCsvImporter(CsvDbModel):
    code = models.CharField()
    description = models.CharField()
    version = models.CharField()
    status = models.CharField()
    start_date = models.DateField()
    end_date = models.DateField()


    class Meta:
        dbModel = CPT
        delimiter = ','


    def ImportFile(self, file_path):
        try:
            my_file = self.import_data(data=open(file_path))
            return True
        except:
            return False


class PayorCsvImporter(CsvDbModel):
    name = models.CharField()
    address1 = models.CharField()
    address2 = models.CharField()
    city = models.CharField()
    state = models.CharField(max_length=2)
    zip_code = models.CharField()
    phone_no = models.CharField()
    status = models.CharField()


    class Meta:
        dbModel = Payor
        delimiter = ','


    def ImportFile(self, file_path):
        try:
            my_file = self.import_data(data=open(file_path))
            return True
        except:
            return False


class TestCsvImporter(CsvDbModel):
    test_name = models.CharField(max_length=50)
    status = models.CharField()
    start_date = models.DateField()
    end_date = models.DateField()
    cpt = models.CharField()


    class Meta:
        dbModel = Test
        delimiter = ','


    def ImportFile(self, file_path):
        try:
            my_file = self.import_data(data=open(file_path))
            return True
        except:
            return False


def move_to_done(src):
    dst = os.path.join(os.path.dirname(os.path.dirname(src)), "done")
    shutil.move(src, dst)

def handle_uploaded_file(f, model):
    file_path = os.path.join(os.getcwd(), "uploads", model, f.name)
    with open(file_path, 'wb+') as destination:
        for chunk in f.chunks():
            destination.write(chunk)

    if model == "ICD":
        myImporter = IcdCsvImporter()
        myImporter.ImportFile(file_path)
    move_to_done(file_path)
