from django.shortcuts import render
from django.views.generic import DetailView, ListView, RedirectView, UpdateView

from .forms import ICDForm, UploadForm, CPTForm, PayorForm, TestForm, PanelForm, PriceSheetForm
from .functions import handle_uploaded_file
from .models import ICD

# Create your views here.
nav = "setup"
sidebar_items = [
        {"name": "icd",
         "url": "setup:home"},
        ]


def setup_home(request):
    context = {
            'nav': nav,
    }
    return render(request, 'setup_home.html', context)


class ICDListView(ListView):
    model = ICD
    list_display = ('__str__', 'is_expired')


def icd_entry(request):
    msg = "Add manually or upload file"
    form = ICDForm(request.POST or None)

    context = {
            "title": "ICD Codes",
            "msg": msg,
            "nav": nav,
            'side': 'icd',
            "sidebar_items": sidebar_items,
            "form": form
    }
    if form.is_valid():
        instance = form.save(commit=False)
        instance.save()
        msg = "Saved"
        context = {
            "msg": msg,
    }
    return render(request, 'forms.html', context)

def upload_file(request):
    msg = "Manually upload file"
    form = UploadForm(request.POST or None)

    context = {
            "title": "File Uploads",
            "msg": msg,
            "nav": nav,
            'side': 'upload',
            "sidebar_items": sidebar_items,
            "form": form
    }
    if request.method == 'POST':
        form = UploadForm(request.POST, request.FILES)
        if form.is_valid():
            model = form.cleaned_data.get("type")
            handle_uploaded_file(request.FILES['file'], model)
            context["msg"] = "Uploaded"
            context["form"] = UploadForm()
            return render(request, 'upload.html', context)
    else:

        form = UploadForm()
        return render(request, 'pages/upload.html', context)
