from django.conf.urls import url

from django.views.generic import TemplateView
from . import views


urlpatterns = [
    url(r'^$', TemplateView.as_view(template_name='pages/setup_home.html'),
        name="home"),
    url(r'^icd/', views.icd_entry, name="icd_entry"),

    url(r'upload/$', views.upload_file, name="file_upload"),

]
